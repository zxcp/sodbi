<?php /** Created by Anton on 26.09.2018. */
$questions = include ('questions.php');
$index = $_POST['index'] ? $_POST['index'] : 1;// Номер текущего вопроса ?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>Sodbi test - questionnaire</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="mx-auto col-sm-5 col-md-4 col-lg-3 col-sm-offset-3 col-md-offset-4" id="col">
                <div id="question">
                    <h2 class="text-center">Вопрос № <?= $index ?></h2>
                    <div class="text-center">
                        <h3><?= $questions[$index]['question'] ?></h3>
                        <?php foreach ($questions[$index]['answers'] as $k => $answer): $k++?>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="answer" id="r-<?= $k ?>" value="<?= $k ?>">
                                <label class="form-check-label" for="r-<?= $k ?>">
                                    <p><?= $answer ?></p>
                                </label>
                            </div>
                        <?php endforeach; ?>
                        <button class="btn btn-primary disabled" type="submit" onclick="get(<?= $index ?>)" aria-disabled="true" disabled>Далее</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript">
    enableBtnOnCheck();
</script>
</body>
</html>