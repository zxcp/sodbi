/** Created by Anton on 27.09.2018. */

function enableBtnOnCheck() {
    $('input[type=radio]').on('click', function () {
        $('button').removeClass('disabled')
            .removeAttr('aria-disabled')
            .removeAttr('disabled');
    });
}
function get(i) {
    $.post( "ajax/question.php", {index: i, answer: $('input[type=radio]:checked').val()}, function(data) {
        $("#question").html(data);
    });
}
function setClassesForResult() {
    $('#col').removeClass().addClass('mx-auto col-sm-6 col-sm-offset-3');
}
