<?php /** Created by Anton on 27.09.2018. */
      /** @var array $wrongAnswers */
      /** @var array $questions */ ?>

<h2 class="text-center">Результат опроса</h2>

<?php if (!$wrongAnswers): ?>
    <h3 class="text-success text-center">Поздравляем! Вы ответили на все вопросы верно!</h3>
<?php else: ?>
    <h3 class="text-danger text-center">Увы. Не все ответы верны.</h3>
    <?php foreach ($wrongAnswers as $index => $answer): ?>
        <div class="text-center">
            <p>Вопрос № <?= $index ?>: <?= $questions[$index]['question'] ?></p>
            <p class="text-danger"><?= $questions[$index]['answers'][--$answer] ?></p>
            <p class="text-success"><?= $questions[$index]['answers'][--$questions[$index]['correctAnswer']] ?></p>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
<script type="text/javascript">
    setClassesForResult();
</script>