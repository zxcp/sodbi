<?php /** Created by Anton on 27.09.2018. */
      /** @var int $index */?>

<h2 class="text-center">Вопрос № <?= $index ?></h2>
<div class="text-center">
    <h3> <?= $questions[$index]['question'] ?> </h3>
    <?php foreach ($questions[$index]['answers'] as $k => $answer): $k++?>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="answer" id="r-<?= $k ?>" value="<?= $k ?>">
            <label class="form-check-label" for="r-<?= $k ?>">
                <p><?= $answer ?></p>
            </label>
        </div>
    <?php endforeach; ?>
    <button class="btn btn-primary disabled" type="submit" onclick="get(<?= $index ?>, $('input[type=radio]:checked').val())" aria-disabled="true">Далее</button>
</div>
<script type="text/javascript">
    enableBtnOnCheck();
</script>