<?php /** Created by Anton on 26.09.2018. */

$questions = include ('../questions.php');
$index = $_POST['index'];
$fileName = '../results/' . $_SERVER['REMOTE_ADDR'] . '.txt';

//Сохраняем ответ, если он не верный
if ($index == 1) {
    if (file_exists($fileName)) unlink($fileName);
    $append = 0;
} else {
    $append = FILE_APPEND;
}
if ($_POST['answer'] != $questions[$index]['correctAnswer']) {
    file_put_contents($fileName, $index . '-' . $_POST['answer'] . ';', $append);
}
//Выводим следующий вопрос или результаты опроса
$index++;
if (!isset($questions[$index])) {
    $statistic = $wrongAnswers = [];
    if (file_exists($fileName)) $statistic = explode(';', file_get_contents($fileName), -1);
    foreach ($statistic as $v) {
        $arr = explode('-', $v);
        $wrongAnswers[$arr[0]] = $arr[1];
    }
    include ('../view/statistic.php');
} else {
    include ('../view/nextQuestion.php');
}